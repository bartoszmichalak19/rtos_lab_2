# Project Title
Field of study: Advanced Applied Electronics <br >
Course: Real Time Operating Systems <br >
Laboratory list no.2.  <br >
author: Bartosz Michalak <br>

## Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Usage](#usage)
- [Tasks description](#tasks)

## About <a name = "about"></a>

This project is created to implement multithread applications for RTOS
laboratory claesses. Apps are written in C++ using existing libraries. 

## Getting Started <a name = "getting_started"></a>

Short introduction just to run code related with tasks. They are divided into three directories, every one has simmilar structure. There is a simple cmake set up and few scripts to run for each task directory. 

### Prerequisites

Preffered linux OS (tested on ubuntu 20.04) and cmake (3.10+ version)


### Building and Running apps <a name = "usage"></a>

Example of usage for one of tasks.

```sh
cd task3
./build_app.sh
./run.sh
```

## Tasks descriptions <a name = "tasks"></a>

**Task1** <br > 
There are two cpp apps, reader and writer. Reader creates named pipe and waits for message. when receives it prints
it on standard output. 
```cpp
//reader 
mkfifo(FIFO_FILE, FIFO_MODE);
  while (1) {
    fd = open(FIFO_FILE, O_RDONLY);
    int length = read(fd, readbuf, sizeof(readbuf));
    if (length > 0) {
      printf("Received message: %s\n", readbuf);
    }
    close(fd);
  }
```
Writer app gets message to write with running argument and write it to the pipe. 
```cpp
//writer
if(argc !=2){
        printf("Nothing passed to communiacte\n");
        exit(1);
    }
    fd = open(FIFO_FILE, O_WRONLY);
    (void)write(fd, argv[1], strlen(argv[1])+1);

    close(fd);
```
Sample of usage is in "run.sh" script, it runs reader in background and then run writer with some string passed as argument. 

```sh
./build/task1_reader &
./build/task1_writer "AAE Bartosz Michalak"
```

**Task 2** 

One simple file was written to show solution. There are two functions :funTX() which allocates shared memory segment, read from input file, measures its size, attach segment to shared memory and unlocks semaphore.
```cpp
void funTX()
{
    sem_wait(&mysem);
    key_t key = ftok("shared_mem_file", 65);
    int shmid = shmget(key, 1024, 0666|IPC_CREAT);
    char *str = (char*) shmat(shmid, (void*)0, 0);
    long input_size;
    char *buffer;
    

    fseek(input, 0, SEEK_END);
    input_size = ftell(input);
    rewind(input);
    buffer=(char*)malloc(sizeof(char)*input_size);
    std::cout << "Input size:" << input_size << "\n";

    int a = fread(buffer, sizeof(char), input_size, input);
    a++;
    memcpy(str, buffer, input_size);
    free(buffer);
    shmdt(str);
    sem_post(&mysem);
}
```
funRX() functiion take semaphore, gets shared memory segment and writes the data to output file, destroys this segment of shared memory  and  unlocks semaphore. 
```cpp
void funRX()
{
    sem_wait(&mysem);
    key_t key = ftok("shared_mem_file", 65);
    int shmid = shmget(key, 1024, 0666|IPC_CREAT);
    char *str = (char*) shmat(shmid, (void*)0, 0);

    printf("Writing!\n");
    fwrite(str, sizeof(char), strlen(str), output);

    shmdt(str);
    shmctl(shmid, IPC_RMID, NULL);
    sem_post(&mysem);
}
```
Simple usage is in main function, it initializes semaphore, opens input file "read_lorem_ipsum.txt" and output file "write_lorem_ipsum.txt". Then starts two threads with funTX and funRX. After executing running script we can observe that output file has 3968 words and is the same as input file.

**Task 3** 

Task consist of pi estimator class that just estimate PI using montecarlo method. Child functionality estimate pi and put its result on the queue. Server function takes and sum results from queue until counter is less than declared sampling threads. Then mean value is calculated and printed on standard output. 

```cpp
void server_fun() {

  while (counter <= number_of_sampling_threads) {
    if (!results.empty()) {
        std::unique_lock<std::mutex> lock(mut);
        pi_mean+=results.front();
        results.pop();
        counter++;
        lock.unlock();
    }
    else{
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
  }
  pi_mean /= number_of_sampling_threads;
  std::cout << "Mean pi value: " << pi_mean << std::endl;
}
```
```cpp
void pi_estimator_child_fun() {
  {
    PiEstimator monte_carlo_pi(number_of_samples);
    std::lock_guard<std::mutex> lck(mut);
    results.push(monte_carlo_pi.calculate());
  }
}
```


