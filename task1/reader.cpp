#include <fcntl.h>
#include <stdio.h>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define FIFO_MODE 0600
#define FIFO_FILE "PIPE"

const unsigned int BUFF_LENGTH = 50;

int main() {
  int fd;
  char readbuf[BUFF_LENGTH];
  mkfifo(FIFO_FILE, FIFO_MODE);
  while (1) {
    fd = open(FIFO_FILE, O_RDONLY);
    int length = read(fd, readbuf, sizeof(readbuf));
    if (length > 0) {
      printf("Received message: %s\n", readbuf);
    }
    close(fd);
  }
  return 0;
}