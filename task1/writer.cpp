#include <stdio.h>
#include <string.h> 
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

#define FIFO_FILE "PIPE"

int main(int argc, char *argv[]){
    int fd;
    if(argc !=2){
        printf("Nothing passed to communiacte\n");
        exit(1);
    }
    fd = open(FIFO_FILE, O_WRONLY);
    (void)write(fd, argv[1], strlen(argv[1])+1);

    close(fd);
    return 0;
}