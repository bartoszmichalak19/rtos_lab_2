#include <iostream>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <semaphore.h>
#include <fstream>
#include <thread>
#include <string>
#include <string.h>

#define SIZE 1000

sem_t mysem;
std::FILE *input;
std::FILE *output;

void funTX()
{
    sem_wait(&mysem);
    key_t key = ftok("shared_mem_file", 65);
    int shmid = shmget(key, 1024, 0666|IPC_CREAT);
    char *str = (char*) shmat(shmid, (void*)0, 0);
    long input_size;
    char *buffer;
    

    fseek(input, 0, SEEK_END);
    input_size = ftell(input);
    rewind(input);
    buffer=(char*)malloc(sizeof(char)*input_size);
    std::cout << "Input size:" << input_size << "\n";

    int a = fread(buffer, sizeof(char), input_size, input);
    a++;
    memcpy(str, buffer, input_size);
    free(buffer);
    shmdt(str);
    sem_post(&mysem);
}

void funRX()
{
    sem_wait(&mysem);
    key_t key = ftok("shared_mem_file", 65);
    int shmid = shmget(key, 1024, 0666|IPC_CREAT);
    char *str = (char*) shmat(shmid, (void*)0, 0);

    printf("Writing!\n");
    fwrite(str, sizeof(char), strlen(str), output);

    shmdt(str);
    shmctl(shmid, IPC_RMID, NULL);
    sem_post(&mysem);
}

int main()
{
    sem_init(&mysem, 0, 1);
    input=fopen("read_lorem_ipsum.txt", "rb");
    output=fopen("write_lorem_ipsum.txt", "wb");

    std::thread t1(funTX);
    std::thread t2(funRX);
    t1.join();
    t2.join();
    sem_destroy(&mysem);

    return 0;
}