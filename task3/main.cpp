#include "pi_estimator.h"
#include "threads_experiment.h"
#include <functional>
#include <iostream>
#include <queue>
#include <thread>
#include <mutex>
#include <chrono>

std::queue<float> results;
std::mutex mut;
const auto number_of_samples = 100000U;
const auto number_of_sampling_threads = 100U;
auto counter = 0U;
float pi_mean = 0.0;
bool whole_threads_sampled = false;

void server_fun() {

  while (counter <= number_of_sampling_threads) {
    if (!results.empty()) {
        std::unique_lock<std::mutex> lock(mut);
        pi_mean+=results.front();
        results.pop();
        counter++;
        lock.unlock();
    }
    else{
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
  }
  pi_mean /= number_of_sampling_threads;
  std::cout << "Mean pi value: " << pi_mean << std::endl;
}

void pi_estimator_child_fun() {
  {
    PiEstimator monte_carlo_pi(number_of_samples);
    std::lock_guard<std::mutex> lck(mut);
    results.push(monte_carlo_pi.calculate());
  }
}

int main() {
  std::function<void()> exp_fun = []() { server_fun(); };
  ThreadsExperiment experiment_one_main_thread(exp_fun, 1);

  exp_fun = []() { pi_estimator_child_fun(); };
  ThreadsExperiment experiment_one(exp_fun, number_of_sampling_threads);

  experiment_one_main_thread.start();
  experiment_one.start();
}
