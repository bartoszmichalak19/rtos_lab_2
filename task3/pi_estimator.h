#ifndef PI_ESTIMATOR_H
#define PI_ESTIMATOR_H

#include <random>
#include <iostream>

class PiEstimator
{
private:
    unsigned int points_quantity_;
    unsigned int points_inside_circle_ = 0;

public:
    PiEstimator(unsigned int points_quantity) : points_quantity_(points_quantity)
    {
        srand(time(NULL));
    }

    float calculate()
    {
        for (unsigned int i = 0; i < points_quantity_; i++)
        {
            auto x = rand() / float(RAND_MAX);
            auto y = rand() / float(RAND_MAX);
            if (x * x + y * y <= 1)
                points_inside_circle_++;
        }

        return 4.0 * points_inside_circle_ / points_quantity_;
    }
};

#endif