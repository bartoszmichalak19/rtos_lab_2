#include <vector>
#include <thread>
#include <functional>

class ThreadsExperiment
{
private:
    std::vector<std::thread> threads;

public:
    ThreadsExperiment(std::function<void()> experimental_fun, const int threads_quantity)
    {
        for (auto i = 0; i < threads_quantity; i++)
        {
            threads.emplace_back(experimental_fun);
        }
    }

    void start()
    {
        for (auto &t : threads)
        {
            if (t.joinable())
            {
                t.join();
            }
        }
    }
};